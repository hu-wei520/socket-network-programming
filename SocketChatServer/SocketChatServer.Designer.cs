﻿namespace SocketChatServer
{
    partial class SocketChatServer
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMsg = new System.Windows.Forms.TextBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.cboUsers = new System.Windows.Forms.ComboBox();
            this.btnZD = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnSendFile = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.BtnStartListing = new System.Windows.Forms.Button();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.btnQunfa = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(13, 431);
            this.txtPath.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(480, 25);
            this.txtPath.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 232);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 15);
            this.label1.TabIndex = 17;
            this.label1.Text = "输入要发送到客户端的消息";
            // 
            // txtMsg
            // 
            this.txtMsg.Location = new System.Drawing.Point(13, 260);
            this.txtMsg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMsg.Multiline = true;
            this.txtMsg.Name = "txtMsg";
            this.txtMsg.Size = new System.Drawing.Size(641, 123);
            this.txtMsg.TabIndex = 16;
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(16, 58);
            this.txtLog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(636, 164);
            this.txtLog.TabIndex = 15;
            // 
            // cboUsers
            // 
            this.cboUsers.FormattingEnabled = true;
            this.cboUsers.Location = new System.Drawing.Point(479, 11);
            this.cboUsers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboUsers.Name = "cboUsers";
            this.cboUsers.Size = new System.Drawing.Size(177, 23);
            this.cboUsers.TabIndex = 14;
            // 
            // btnZD
            // 
            this.btnZD.Location = new System.Drawing.Point(597, 396);
            this.btnZD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnZD.Name = "btnZD";
            this.btnZD.Size = new System.Drawing.Size(87, 26);
            this.btnZD.TabIndex = 9;
            this.btnZD.Text = "发送震动";
            this.btnZD.UseVisualStyleBackColor = true;
            this.btnZD.Click += new System.EventHandler(this.BtnZD_Click);
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(503, 396);
            this.btnSend.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(87, 26);
            this.btnSend.TabIndex = 10;
            this.btnSend.Text = "发送消息";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.BtnSend_Click);
            // 
            // btnSendFile
            // 
            this.btnSendFile.Location = new System.Drawing.Point(597, 431);
            this.btnSendFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSendFile.Name = "btnSendFile";
            this.btnSendFile.Size = new System.Drawing.Size(87, 26);
            this.btnSendFile.TabIndex = 11;
            this.btnSendFile.Text = "发送文件";
            this.btnSendFile.UseVisualStyleBackColor = true;
            this.btnSendFile.Click += new System.EventHandler(this.BtnSendFile_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(503, 430);
            this.btnSelect.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(87, 26);
            this.btnSelect.TabIndex = 12;
            this.btnSelect.Text = "选择文件";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // BtnStartListing
            // 
            this.BtnStartListing.Location = new System.Drawing.Point(333, 10);
            this.BtnStartListing.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnStartListing.Name = "BtnStartListing";
            this.BtnStartListing.Size = new System.Drawing.Size(113, 26);
            this.BtnStartListing.TabIndex = 13;
            this.BtnStartListing.Text = "开始监听";
            this.BtnStartListing.UseVisualStyleBackColor = true;
            this.BtnStartListing.Click += new System.EventHandler(this.BtnStartListing_Click);
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(203, 10);
            this.txtPort.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(100, 25);
            this.txtPort.TabIndex = 7;
            this.txtPort.Text = "50000";
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(16, 10);
            this.txtIP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(156, 25);
            this.txtIP.TabIndex = 8;
            this.txtIP.Text = "192.168.109.114";
            // 
            // btnQunfa
            // 
            this.btnQunfa.Location = new System.Drawing.Point(408, 396);
            this.btnQunfa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnQunfa.Name = "btnQunfa";
            this.btnQunfa.Size = new System.Drawing.Size(87, 26);
            this.btnQunfa.TabIndex = 10;
            this.btnQunfa.Text = "群发消息";
            this.btnQunfa.UseVisualStyleBackColor = true;
            this.btnQunfa.Click += new System.EventHandler(this.BtnQunfa_Click);
            // 
            // SocketChatServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 491);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtMsg);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.cboUsers);
            this.Controls.Add(this.btnZD);
            this.Controls.Add(this.btnQunfa);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.btnSendFile);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.BtnStartListing);
            this.Controls.Add(this.txtPort);
            this.Controls.Add(this.txtIP);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SocketChatServer";
            this.Text = "Scoket聊天服务端";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMsg;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.ComboBox cboUsers;
        private System.Windows.Forms.Button btnZD;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnSendFile;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button BtnStartListing;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.Button btnQunfa;
    }
}

